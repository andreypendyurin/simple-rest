Simple REST backend

Rebuild project:
1. Install Gradle on local PC.
2. Run command 'gradle build'.
3. Find executable jar in $PROJECT_ROOT\build\libs

Run app:
1. Make sure that Java Runtime Environment 1.8 has been installed on your PC.
1. Execute 'java -jar simple-rest-1.0.0.jar'