package com.il.api;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.il.entities.GetEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Andrey Pendyurin on 07.06.2017.
 */

@RestController
public class SimpleEndpoint {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetEntity getEntity(@RequestParam Map<String,String> allRequestParams, @PathVariable("id") String id){
        Joiner.MapJoiner mapJoiner = Joiner.on(",").withKeyValueSeparator("=");
        String log = String.format("Requested GET /{id} with 'path parameter' %s and parameters %s", id, mapJoiner.join(allRequestParams));
        System.out.println(log);
        return new GetEntity("my@mail.ru", "John", "Smith");
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> postEntity(@RequestParam Map<String,String> allRequestParams, @RequestBody GetEntity entity){
        Joiner.MapJoiner mapJoiner = Joiner.on(",").withKeyValueSeparator("=");
        Gson g = new Gson();

        String log = String.format("Requested POST / with body %s and parameters %s", g.toJson(entity), mapJoiner.join(allRequestParams));
        System.out.println(log);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> putEntity(@RequestParam Map<String,String> allRequestParams, @PathVariable("id") String id, @RequestBody GetEntity entity){
        Joiner.MapJoiner mapJoiner = Joiner.on(",").withKeyValueSeparator("=");
        Gson g = new Gson();

        System.out.println(id);

        String log = String.format("Requested PUT /{id} with 'path parameter' %s and  body %s and parameters %s", id, g.toJson(entity), mapJoiner.join(allRequestParams));
        System.out.println(log);
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteEntity(@RequestParam Map<String,String> allRequestParams, @PathVariable("id") String id){
        Joiner.MapJoiner mapJoiner = Joiner.on(",").withKeyValueSeparator("=");

        String log = String.format("Requested DELETE /{id}  with 'path parameter' %s and parameters %s", id, mapJoiner.join(allRequestParams));
        System.out.println(log);

        return ResponseEntity.noContent().build();
    }
}
