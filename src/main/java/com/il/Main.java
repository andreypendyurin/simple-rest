package com.il;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Andrey Pendyurin on 06.06.2017.
 */


@SpringBootApplication(scanBasePackages = "com.il.api")
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
